# Plantuml

This is my personal diagram scheme and examples using [PlantUML](https://plantuml.com).

## Usage

There are many ways to render PlantUML diagrams, my favourite at the moment is Visual Studio Code with the PlantUML extension.

## Examples

Included in this repository are some examples I often use when doing network design.
